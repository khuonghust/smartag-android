package com.kdteam.smartag.fragment;

import android.webkit.WebResourceRequest;
import android.webkit.WebResourceResponse;
import android.webkit.WebView;
import android.webkit.WebViewClient;

class MyBrowser extends WebViewClient {
    @Override
    public boolean shouldOverrideUrlLoading(WebView view, String url) {
        view.loadUrl(url);
        return true;
    }
    @Override
    public void onReceivedHttpError (WebView view, WebResourceRequest request, WebResourceResponse errorResponse) {
//            Toast.makeText(view.getContext(), "Không thể tải dữ liệu từ camera", Toast.LENGTH_LONG).show();
    }


}
