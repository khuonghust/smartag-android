package com.kdteam.smartag.fragment;


import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.jjoe64.graphview.DefaultLabelFormatter;
import com.jjoe64.graphview.GraphView;
import com.jjoe64.graphview.series.DataPoint;
import com.jjoe64.graphview.series.LineGraphSeries;
import com.jjoe64.graphview.series.Series;
import com.kdteam.smartag.R;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class HistoryFragment extends Fragment {


    private DatabaseReference mFirebaseDatabase;
    private FirebaseDatabase mFirebaseInstance;
    @SuppressLint("SimpleDateFormat") final SimpleDateFormat m_sdf = new SimpleDateFormat("hh:mm:ss");


    Date mDate;
    TextView mDateTime;
    GraphView m_graphView_air_temp;
    GraphView m_graphView_air_humidity;
    GraphView m_graphView_water_temp;
    GraphView m_graphView_light;
    GraphView m_graphView_co2;
    GraphView m_graphView_ph;
    GraphView m_graphView_ec;

    LineGraphSeries<DataPoint> m_series_air_temp;
    LineGraphSeries<DataPoint> m_series_air_humidity;
    LineGraphSeries<DataPoint> m_series_water_temp;
    LineGraphSeries<DataPoint> m_series_light;
    LineGraphSeries<DataPoint> m_series_co2;
    LineGraphSeries<DataPoint> m_series_ph;
    LineGraphSeries<DataPoint> m_series_ec;

    public HistoryFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_history, container, false);
        mFirebaseInstance = FirebaseDatabase.getInstance();
        setupUi(view);
        setupFirebase(view);
        return view;
    }

    private void setupFirebase(View view) {
        Date dNow = new Date( );
        @SuppressLint("SimpleDateFormat") SimpleDateFormat ft = new SimpleDateFormat ("yyyy-MM-dd");

        String dateNow= ft.format(dNow);

        mFirebaseInstance.getReference("sensor/"+ dateNow + "/reported").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                List<SensorReported> listSensorReported = new ArrayList<>();
                for (DataSnapshot uniqueSnapshot : dataSnapshot.getChildren()) {
                    if(uniqueSnapshot.hasChild("time_of_day"))
                    {
                        SensorReported sensor = uniqueSnapshot.getValue(SensorReported.class);
                        listSensorReported.add(sensor);
                    }
                }

                if(!listSensorReported.isEmpty())
                    updateChart(listSensorReported);

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    private static DecimalFormat REAL_FORMATTER = new DecimalFormat("0.#");

    private void updateChart(List<SensorReported> listSensorReported) {
        int count = listSensorReported.size();
        DataPoint[] dp_air_temp = new DataPoint[count];
        DataPoint[] dp_air_humidity = new DataPoint[count];
        DataPoint[] dp_water_temp = new DataPoint[count];
        DataPoint[] dp_light = new DataPoint[count];
        DataPoint[] dp_co2 = new DataPoint[count];
        DataPoint[] dp_ph = new DataPoint[count];
        DataPoint[] dp_ec = new DataPoint[count];
        int index = 0;

        for (SensorReported sensor : listSensorReported){
            dp_air_temp[index] = new DataPoint(sensor.time_of_day, sensor.air_temperature_celsius);
            dp_air_humidity[index] = new DataPoint(sensor.time_of_day, sensor.air_humidity_percent);
            dp_water_temp[index] = new DataPoint(sensor.time_of_day, sensor.water_temperature_celsius);
            dp_light[index] = new DataPoint(sensor.time_of_day, sensor.light_illumination_distance_cm);
            dp_co2[index] = new DataPoint(sensor.time_of_day, sensor.air_carbon_dioxide_ppm);
            dp_ph[index] = new DataPoint(sensor.time_of_day, sensor.water_potential_hydrogen);
            dp_ec[index] = new DataPoint(sensor.time_of_day, sensor.water_electrical_conductivity_ms_cm);

            index++;
        }



        updateViewport(m_graphView_air_temp, listSensorReported, 0, 100);
        updateViewport(m_graphView_air_humidity, listSensorReported, 0, 100);
        updateViewport(m_graphView_water_temp, listSensorReported, 0, 100);
        updateViewport(m_graphView_light, listSensorReported, 0, 188000);
        updateViewport(m_graphView_co2, listSensorReported, 0, 2000);
        updateViewport(m_graphView_ph, listSensorReported, 0, 14);
        updateViewport(m_graphView_ec, listSensorReported, 0, 20);

        m_series_air_temp.resetData(dp_air_temp);
        m_series_air_humidity.resetData(dp_air_humidity);
        m_series_water_temp.resetData(dp_water_temp);
        m_series_light.resetData(dp_light);
        m_series_co2.resetData(dp_co2);
        m_series_ph.resetData(dp_ph);
        m_series_ec.resetData(dp_ec);

    }

    private void initGraph(GraphView graphView)
    {
        graphView.setPadding(30, 0, 0, 0);
        graphView.getViewport().setScalable(true);
        graphView.getViewport().setScalableY(true);


        graphView.getGridLabelRenderer().setNumHorizontalLabels(4);
        graphView.getGridLabelRenderer().setLabelFormatter(new DefaultLabelFormatter(){
            @Override
            public String formatLabel(double value, boolean isValueX){
                if(isValueX){
                    int hours = (int) (value / 3600);
                    int minutes = (int) ((value % 3600) / 60);
                    return String.format("%02d:%02d", hours, minutes);
                }else{
                    return super.formatLabel(value, isValueX);
                }
            }
        });
    }
    private void setupUi(View view) {

        Button btnExport = view.findViewById(R.id.btnExport);
        btnExport.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getContext(), "Exported to file", Toast.LENGTH_SHORT).show();
            }
        });
        mDateTime = view.findViewById(R.id.dateTime);
        mDateTime.setText("Select day");
        m_graphView_air_temp= view.findViewById(R.id.graphAirTemperature);
        m_graphView_air_humidity = view.findViewById(R.id.graphAirHumidity);
        m_graphView_water_temp = view.findViewById(R.id.graphWaterTemperature);
        m_graphView_light = view.findViewById(R.id.graphLight);
        m_graphView_co2 = view.findViewById(R.id.graphCO2);
        m_graphView_ec = view.findViewById(R.id.graphEC);
        m_graphView_ph = view.findViewById(R.id.graphpH);


        m_series_air_temp = new LineGraphSeries<DataPoint>();
        m_series_air_humidity = new LineGraphSeries<DataPoint>();
        m_series_water_temp = new LineGraphSeries<>();
        m_series_light = new LineGraphSeries<>();
        m_series_co2 = new LineGraphSeries<>();
        m_series_ph = new LineGraphSeries<>();
        m_series_ec = new LineGraphSeries<>();

        initSerial(m_series_air_temp);
        initSerial(m_series_air_humidity);
        initSerial(m_series_water_temp);
        initSerial(m_series_light);
        initSerial(m_series_co2);
        initSerial(m_series_ph);
        initSerial(m_series_ec);

        m_graphView_air_temp.addSeries(m_series_air_temp);
        m_graphView_air_humidity.addSeries(m_series_air_humidity);
        m_graphView_water_temp.addSeries(m_series_water_temp);
        m_graphView_light.addSeries(m_series_light);
        m_graphView_co2.addSeries(m_series_co2);
        m_graphView_ph.addSeries(m_series_ph);
        m_graphView_ec.addSeries(m_series_ec);


        initGraph(m_graphView_air_temp);
        initGraph(m_graphView_air_humidity);
        initGraph(m_graphView_water_temp);
        initGraph(m_graphView_light);
        initGraph(m_graphView_co2);
        initGraph(m_graphView_ph);
        initGraph(m_graphView_ec);

        mDateTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // calender class's instance and get current date , month and year from calender
                final Calendar c = Calendar.getInstance();
                mDate = c.getTime();
                int mYear = c.get(Calendar.YEAR); // current year
                int mMonth = c.get(Calendar.MONTH); // current month
                int mDay = c.get(Calendar.DAY_OF_MONTH); // current day
                // date picker dialog
                DatePickerDialog datePickerDialog = new DatePickerDialog(getContext(),
                        new DatePickerDialog.OnDateSetListener() {

                            @SuppressLint("SetTextI18n")
                            @Override
                            public void onDateSet(DatePicker view, int year,
                                                  int monthOfYear, int dayOfMonth) {
                                // set day of month , month and year value in the edit text

                                String value = String.format("%d-%02d-%02d", year, monthOfYear+1,dayOfMonth);

                                mDateTime.setText(value);

                                mFirebaseInstance.getReference("sensor/"+ value + "/reported").addListenerForSingleValueEvent(new ValueEventListener() {
                                    @Override
                                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                        List<SensorReported> listSensorReported = new ArrayList<>();
                                        for (DataSnapshot uniqueSnapshot : dataSnapshot.getChildren()) {
                                            if(uniqueSnapshot.hasChild("time_of_day"))
                                            {
                                                SensorReported sensor = uniqueSnapshot.getValue(SensorReported.class);
                                                listSensorReported.add(sensor);
                                            }
                                        }

//                                        if(!listSensorReported.isEmpty())
                                            updateChart(listSensorReported);
                                    }

                                    @Override
                                    public void onCancelled(@NonNull DatabaseError databaseError) {

                                    }
                                });

                            }
                        }, mYear, mMonth, mDay);
                datePickerDialog.show();
            }
        });
    }

    private void updateViewport(GraphView graphView, List<SensorReported> listSensorReported, double minY, double maxY){
        graphView.getViewport().setXAxisBoundsManual(true);
        graphView.getViewport().setYAxisBoundsManual(true);
        graphView.getGridLabelRenderer().setHumanRounding(true);
        graphView.getGridLabelRenderer().setPadding(50);
//        graphView.getGridLabelRenderer().setHorizontalLabelsAngle(135);
        if(listSensorReported.size()>10) {
            graphView.getViewport().setMinX(listSensorReported.get(listSensorReported.size() - 10).time_of_day);
            graphView.getViewport().setMaxX(listSensorReported.get(listSensorReported.size() - 1).time_of_day + 10);
            graphView.getViewport().setMinY(minY);
            graphView.getViewport().setMaxY(maxY);
            graphView.getGridLabelRenderer().setNumHorizontalLabels(3);
        }
    }

    private void initSerial(LineGraphSeries<DataPoint> series) {
//        series.set
        series.setDrawBackground(true);
        series.setDrawDataPoints(true);
        series.setAnimated(true);
    }

}
