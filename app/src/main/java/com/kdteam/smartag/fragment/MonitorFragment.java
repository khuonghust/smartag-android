package com.kdteam.smartag.fragment;


import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.kdteam.smartag.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * A simple {@link Fragment} subclass.
 */
public class MonitorFragment extends Fragment {

    private DatabaseReference mFirebaseDatabase;
    private FirebaseDatabase mFirebaseInstance;
    private String mDeviceId = "my-rasp-openag";
    public MonitorFragment() {
        // Required empty public constructor
    }
    private WebView m_cameraView;

    MyBrowser m_web = new MyBrowser();
    ProgressBar m_prog_air_temp;
    ProgressBar m_prog_air_humidity;
    ProgressBar m_prog_water_temp;
    ProgressBar m_prog_light;
    ProgressBar m_prog_co2;
    ProgressBar m_prog_ph;
    ProgressBar m_prog_ec;

    TextView m_txt_air_temp;
    TextView m_txt_air_humidity;
    TextView m_txt_water_temp;
    TextView m_txt_light;
    TextView m_txt_co2;
    TextView m_txt_ph;
    TextView m_txt_ec;





    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_monitor, container, false);
        mFirebaseInstance = FirebaseDatabase.getInstance();
        setupUi(view);
        setupFirebase(view);

        return view;
    }

    private void setupFirebase(final View view) {
        Date dNow = new Date( );
        @SuppressLint("SimpleDateFormat") SimpleDateFormat ft = new SimpleDateFormat ("yyyy-MM-dd");

        String dateNow= ft.format(dNow);
        Query lastchield = mFirebaseInstance.getReference("sensor/"+ dateNow + "/reported").orderByKey().limitToLast(1);
        lastchield.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

            }

            @Override
            public void onChildChanged(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
                SensorReported sensorReported = dataSnapshot.getValue(SensorReported.class);
                assert sensorReported != null;
                updateSensorReported(view, sensorReported);
            }

            @Override
            public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        mFirebaseInstance.getReference(mDeviceId + "/ipCamera").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                String ipCam = dataSnapshot.getValue().toString();
                m_cameraView.loadUrl("http://" +ipCam + "/html/video.php");
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }
    private static DecimalFormat REAL_FORMATTER = new DecimalFormat("0.#");
    private void updateSensorReported(View view, SensorReported sensorReported)
    {


        m_txt_air_temp.setText(String.valueOf(REAL_FORMATTER.format(sensorReported.air_temperature_celsius)));
        m_txt_air_humidity.setText(String.valueOf(REAL_FORMATTER.format(sensorReported.air_humidity_percent)));
        m_txt_water_temp.setText(String.valueOf(REAL_FORMATTER.format(sensorReported.water_temperature_celsius)));
        m_txt_light.setText(String.valueOf(REAL_FORMATTER.format(sensorReported.light_illumination_distance_cm)));
        m_txt_co2.setText(String.valueOf(REAL_FORMATTER.format(sensorReported.air_carbon_dioxide_ppm)));
        m_txt_ph.setText(String.valueOf(REAL_FORMATTER.format(sensorReported.water_potential_hydrogen)));
        m_txt_ec.setText(String.valueOf(REAL_FORMATTER.format(sensorReported.water_electrical_conductivity_ms_cm)));


        m_prog_air_temp.setProgress(sensorReported.get_air_temp_percent(), true);
        m_prog_air_humidity.setProgress(sensorReported.get_air_humidity_percent(), true);
        m_prog_water_temp.setProgress(sensorReported.get_water_temp_percent(), true);
        m_prog_light.setProgress(sensorReported.get_light_percent(), true);
        m_prog_co2.setProgress(sensorReported.get_co2_percent(), true);
        m_prog_ph.setProgress(sensorReported.get_ph_percent(), true);
        m_prog_ec.setProgress(sensorReported.get_ec_percent(), true);
    }
    @SuppressLint("SetJavaScriptEnabled")
    private void setupUi(View view) {
        m_prog_air_temp = (ProgressBar) view.findViewById(R.id.prog_air_temp);
        m_prog_air_humidity = (ProgressBar) view.findViewById(R.id.prog_air_humidity);
        m_prog_water_temp = (ProgressBar) view.findViewById(R.id.prog_water_temp);
        m_prog_light = (ProgressBar) view.findViewById(R.id.prog_light);
        m_prog_co2 = (ProgressBar) view.findViewById(R.id.prog_co2);
        m_prog_ph = (ProgressBar) view.findViewById(R.id.prog_pH);
        m_prog_ec = (ProgressBar) view.findViewById(R.id.prog_ec);

        m_txt_air_temp = (TextView) view.findViewById(R.id.txt_air_temp);
        m_txt_air_humidity = (TextView) view.findViewById(R.id.txt_air_humidity);
        m_txt_water_temp = (TextView) view.findViewById(R.id.txt_water_temp);
        m_txt_light = (TextView) view.findViewById(R.id.txt_light);
        m_txt_co2 = (TextView) view.findViewById(R.id.txt_co2);
        m_txt_ph = (TextView) view.findViewById(R.id.txt_ph);
        m_txt_ec = (TextView) view.findViewById(R.id.txt_ec);

        m_cameraView = (WebView) view.findViewById(R.id.cameraView);
        m_cameraView.setWebViewClient(m_web);
        m_cameraView.getSettings().setLoadsImagesAutomatically(true);
        m_cameraView.getSettings().setJavaScriptEnabled(true);
        m_cameraView.setScrollBarStyle(View.SCROLLBARS_INSIDE_OVERLAY);
        m_cameraView.loadUrl("http://192.168.1.25/html/video.php");

    }

}
