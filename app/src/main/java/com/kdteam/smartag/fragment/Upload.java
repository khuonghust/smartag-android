package com.kdteam.smartag.fragment;

public class Upload {
    public String date;
    public String url;

    public Upload() {
        //empty constructor needed
    }

    public Upload(String date, String url) {
        this.date = date;
        this.url = url;
    }
}