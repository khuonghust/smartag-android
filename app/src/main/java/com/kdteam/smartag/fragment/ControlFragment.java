package com.kdteam.smartag.fragment;


import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.SeekBar;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.kdteam.smartag.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.lang.reflect.Array;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

/**
 * A simple {@link Fragment} subclass.
 */
public class ControlFragment extends Fragment {

    private DatabaseReference mFirebaseDatabase;
    private FirebaseDatabase mFirebaseInstance;

    String m_deviceId = "my-rasp-openag";
    Switch m_switchMotor1;
    Switch m_switchMotor2;
    Switch m_switchMotor3;
    Switch m_switchLight;

    List<Boolean> m_IsSwitchMotor = Arrays.asList(true, true, true);
    Boolean m_IsSwitchLightFirstCall = true;
    Integer m_isSpinnerLightFirstCall = 0;
    Boolean m_isButtonAutoFirstCall = true;
    Boolean m_isButtonManualFirstCall = true;

    TextView m_textMotorSpeed1;
    TextView m_textMotorSpeed2;
    TextView m_textMotorSpeed3;
    TextView m_textLightPower;
    TextView m_textLightColor;

    SeekBar m_seekBarMotor1;
    SeekBar m_seekBarMotor2;
    SeekBar m_seekBarMotor3;
    SeekBar m_seekBarLight;

    Spinner m_spinnerRecipe, m_spinnerLight;

    Button m_loadRecipe, m_infoRecipe, m_manageRecipe;
    Button m_buttonAuto;
    Button m_buttonManual;

    List<RecipeClass> m_myRecipeList = new ArrayList<>();
    private static DecimalFormat REAL_FORMATTER = new DecimalFormat("0.#");

    public ControlFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_control, container, false);
        mFirebaseInstance = FirebaseDatabase.getInstance();
        setupUi(view);

        return view;
    }

    private void setupUi(View view) {
        m_switchMotor1 = view.findViewById(R.id.switchMotor1);
        m_switchMotor2 = view.findViewById(R.id.switchMotor2);
        m_switchMotor3 = view.findViewById(R.id.switchMotor3);
        m_switchLight = view.findViewById(R.id.switchLight);

        m_textMotorSpeed1 = view.findViewById(R.id.textMotorSpeed1);
        m_textMotorSpeed2 = view.findViewById(R.id.textMotorSpeed2);
        m_textMotorSpeed3 = view.findViewById(R.id.textMotorSpeed3);
        m_textLightPower = view.findViewById(R.id.textLightPower);
        m_textLightColor = view.findViewById(R.id.textLightColor);

        m_seekBarMotor1 = view.findViewById(R.id.seekBarMotor1);
        m_seekBarMotor2 = view.findViewById(R.id.seekBarMotor2);
        m_seekBarMotor3 = view.findViewById(R.id.seekBarMotor3);
        m_seekBarLight = view.findViewById(R.id.seekBarLight);

        m_spinnerRecipe = view.findViewById(R.id.spinnerRecipes);
        m_spinnerLight = view.findViewById(R.id.spinnerLightColor);
        updateListRecipeFromDatabase(m_spinnerRecipe);



        // initi
        mFirebaseInstance.getReference(String.format("%s/curr_actuator", m_deviceId)).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

//                m_isUpdate = false;
                int stateFan1 = dataSnapshot.child("fan_channel_1/state").getValue(Integer.class);
                int stateFan2 = dataSnapshot.child("fan_channel_2/state").getValue(Integer.class);
                int stateFan3 = dataSnapshot.child("fan_channel_3/state").getValue(Integer.class);
                int pwmFan1 = dataSnapshot.child("fan_channel_1/pwm").getValue(Integer.class);
                int pwmFan2 = dataSnapshot.child("fan_channel_2/pwm").getValue(Integer.class);
                int pwmFan3 = dataSnapshot.child("fan_channel_3/pwm").getValue(Integer.class);

                Boolean stateLed = dataSnapshot.child("light/state").getValue(Boolean.class);
                int pwmLed = dataSnapshot.child("light/pwm").getValue(Integer.class);
                int indexled = dataSnapshot.child("light/index").getValue(Integer.class);

                m_seekBarMotor1.setProgress(convertToProgress(pwmFan1));
                m_seekBarMotor2.setProgress(convertToProgress(pwmFan2));
                m_seekBarMotor3.setProgress(convertToProgress(pwmFan3));
                m_seekBarLight.setProgress(convertToProgress(pwmLed));

                m_seekBarMotor1.setEnabled(stateFan1 == 1);
                m_seekBarMotor2.setEnabled(stateFan2 == 1);
                m_seekBarMotor3.setEnabled(stateFan3 == 1);
                m_switchMotor1.setChecked(stateFan1 == 1);
                m_switchMotor2.setChecked(stateFan2 == 1);
                m_switchMotor3.setChecked(stateFan3 == 1);
                m_seekBarLight.setEnabled(stateLed);
                m_spinnerLight.setSelection(indexled);
                m_switchLight.setChecked(stateLed);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        mFirebaseInstance.getReference(String.format("%s/curr_recipe", m_deviceId)).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                int indexRecipe = dataSnapshot.child("index").getValue(Integer.class);
                m_spinnerRecipe.setSelection(indexRecipe);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        mFirebaseInstance.getReference(String.format("%s/curr_mode", m_deviceId)).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                int mode = dataSnapshot.getValue(Integer.class);
                if(mode == 1)
                    m_buttonAuto.callOnClick();
                else
                    m_buttonManual.callOnClick();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        updateWhenSwitch(m_switchMotor1, "fan_channel_1", m_seekBarMotor1, 1, 0);
        updateWhenSwitch(m_switchMotor2, "fan_channel_2", m_seekBarMotor2, 2, 1);
        updateWhenSwitch(m_switchMotor3, "fan_channel_3", m_seekBarMotor3, 3, 2);
        m_switchLight.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(!m_IsSwitchLightFirstCall)
                {
                    if(isChecked){
                        try {
                            updateConfig("light_channel_" + String.valueOf(m_spinnerLight.getSelectedItemPosition() + 1) + "_output_percents",
                                    convertToPwm(m_seekBarLight.getProgress()),
                                    m_spinnerLight.getSelectedItemPosition());
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        m_seekBarLight.setEnabled(true);
                        m_spinnerLight.setEnabled(true);
                    }
                    else{
                        try {
                            updateConfig("light_channel_" + String.valueOf(m_spinnerLight.getSelectedItemPosition() + 1) + "_output_percents", 0,
                                    m_spinnerLight.getSelectedItemPosition());
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        m_seekBarLight.setEnabled(false);
                        m_spinnerLight.setEnabled(false);
                    }

                    String ref = String.format("%s/curr_actuator/light", m_deviceId);
                    mFirebaseInstance.getReference(ref).child("pwm").setValue(convertToPwm(m_seekBarLight.getProgress()));
                    mFirebaseInstance.getReference(ref).child("state").setValue(m_switchLight.isChecked());
                    mFirebaseInstance.getReference(ref).child("variable").setValue("light_channel_" + String.valueOf(m_spinnerLight.getSelectedItemPosition() + 1) + "_output_percents");
                    mFirebaseInstance.getReference(ref).child("index").setValue(m_spinnerLight.getSelectedItemPosition());
                }
                m_IsSwitchLightFirstCall = false;
            }
        });

        updateWhenChangeSeekBar(m_seekBarMotor1, m_textMotorSpeed1, "fan_channel_1", 1);
        updateWhenChangeSeekBar(m_seekBarMotor2, m_textMotorSpeed2, "fan_channel_2", 2);
        updateWhenChangeSeekBar(m_seekBarMotor3, m_textMotorSpeed3, "fan_channel_3", 3);

//        setupButtonOnOff(m_switchLight)

        List<String> listLed = new ArrayList<>();
        listLed.add(getResources().getString(R.string.lightColor1));
        listLed.add(getResources().getString(R.string.lightColor2));
        listLed.add(getResources().getString(R.string.lightColor3));
        listLed.add(getResources().getString(R.string.lightColor4));
        listLed.add(getResources().getString(R.string.lightColor5));
        listLed.add(getResources().getString(R.string.lightColor6));
        listLed.add(getResources().getString(R.string.lightColor7));
        updateSpinner(m_spinnerLight, listLed);


        m_loadRecipe = view.findViewById(R.id.buttonLoad);
        m_infoRecipe = view.findViewById(R.id.buttonInfo);

//        m_spinnerLight.setOnTouchListener(View.OnTouchListener);

        m_spinnerLight.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                String variable = null;
                switch (position) {
                    case 0:
                        m_textLightColor.setBackgroundColor(getResources().getColor(R.color.color1, getActivity().getTheme()));
                        variable = "light_channel_1_output_percents";
                        break;
                    case 1:
                        m_textLightColor.setBackgroundColor(getResources().getColor(R.color.color2, getActivity().getTheme()));
                        variable = "light_channel_2_output_percents";
                        break;
                    case 2:
                        m_textLightColor.setBackgroundColor(getResources().getColor(R.color.color3, getActivity().getTheme()));
                        variable = "light_channel_3_output_percents";
                        break;
                    case 3:
                        m_textLightColor.setBackgroundColor(getResources().getColor(R.color.color4, getActivity().getTheme()));
                        variable = "light_channel_4_output_percents";
                        break;
                    case 4:
                        m_textLightColor.setBackgroundColor(getResources().getColor(R.color.color5, getActivity().getTheme()));
                        variable = "light_channel_5_output_percents";
                        break;
                    case 5:
                        m_textLightColor.setBackgroundColor(getResources().getColor(R.color.color6, getActivity().getTheme()));
                        variable = "light_channel_6_output_percents";
                        break;
                    case 6:
                        m_textLightColor.setBackgroundColor(getResources().getColor(R.color.color7, getActivity().getTheme()));
                        variable = "light_channel_7_output_percents";
                        break;
                }


                if(m_isSpinnerLightFirstCall>1)
                {
                    try {
                        updateConfig(variable, convertToPwm(m_seekBarLight.getProgress()), 0);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    String ref = String.format("%s/curr_actuator/light", m_deviceId);
                    mFirebaseInstance.getReference(ref).child("pwm").setValue(convertToPwm(m_seekBarLight.getProgress()));
                    mFirebaseInstance.getReference(ref).child("state").setValue(m_switchLight.isChecked());
                    mFirebaseInstance.getReference(ref).child("variable").setValue(variable);
                    mFirebaseInstance.getReference(ref).child("index").setValue(m_spinnerLight.getSelectedItemPosition());
                }
                m_isSpinnerLightFirstCall++;
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
            }
        });

        m_buttonAuto = view.findViewById(R.id.buttonAuto);
        m_buttonManual = view.findViewById(R.id.buttonManual);

       m_buttonAuto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                m_seekBarMotor1.setAlpha(.5f);
                m_seekBarMotor1.setEnabled(false);
                m_seekBarMotor2.setAlpha(.5f);
                m_seekBarMotor2.setEnabled(false);
                m_seekBarMotor3.setAlpha(.5f);
                m_seekBarMotor3.setEnabled(false);
                m_seekBarLight.setAlpha(.5f);
                m_seekBarLight.setEnabled(false);
                m_switchMotor1.setAlpha(.5f);
                m_switchMotor1.setClickable(false);
                m_switchMotor2.setAlpha(.5f);
                m_switchMotor2.setClickable(false);
                m_switchMotor3.setAlpha(.5f);
                m_switchMotor3.setClickable(false);
                m_switchLight.setAlpha(.5f);
                m_switchLight.setClickable(false);
                m_spinnerLight.setAlpha(.5f);
                m_spinnerLight.setClickable(false);

                if(!m_isButtonAutoFirstCall)
                {
                    try {
                        updateMode("auto");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    String ref = String.format("%s/curr_mode", m_deviceId);
                    mFirebaseInstance.getReference(ref).setValue(1);
                }
                m_isButtonAutoFirstCall = false;
            }
        });

	m_buttonManual.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                m_seekBarMotor1.setAlpha(1.0f);
                m_seekBarMotor1.setEnabled(true);
                m_seekBarMotor2.setAlpha(1.0f);
                m_seekBarMotor2.setEnabled(true);
                m_seekBarMotor3.setAlpha(1.0f);
                m_seekBarMotor3.setEnabled(true);
                m_seekBarLight.setAlpha(1.0f);
                m_seekBarLight.setEnabled(true);
                m_switchMotor1.setAlpha(1.0f);
                m_switchMotor1.setClickable(true);
                m_switchMotor2.setAlpha(1.0f);
                m_switchMotor2.setClickable(true);
                m_switchMotor3.setAlpha(1.0f);
                m_switchMotor3.setClickable(true);
                m_switchLight.setAlpha(1.0f);
                m_switchLight.setClickable(true);
                m_spinnerLight.setAlpha(1.0f);
                m_spinnerLight.setClickable(true);
                if(!m_isButtonManualFirstCall)
                {
                    try {
                        updateMode("manual");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    String ref = String.format("%s/curr_mode", m_deviceId);
                    mFirebaseInstance.getReference(ref).setValue(0);
                }
                m_isButtonManualFirstCall = false;
            }
        });

        // load recipe
        m_loadRecipe.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String variable = m_myRecipeList.get(m_spinnerRecipe.getSelectedItemPosition()).uuid;
                String value = m_myRecipeList.get(m_spinnerRecipe.getSelectedItemPosition()).name;
                try {
                    updateRecipe(variable, value, m_spinnerRecipe.getSelectedItemPosition());
                    Toast.makeText(getContext(), "Load recipe successful", Toast.LENGTH_SHORT).show();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });

        // info recipe
        m_infoRecipe.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });



        m_seekBarLight.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                m_textLightPower.setText(String.valueOf((int) (progress/65535.0 * 100)) +"%");

            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                try {
                    updateConfig("light_channel_" + String.valueOf(m_spinnerLight.getSelectedItemPosition() + 1) + "_output_percents",
                            convertToPwm(seekBar.getProgress()), m_spinnerLight.getSelectedItemPosition());

                    String ref = String.format("%s/curr_actuator/light", m_deviceId);
                    mFirebaseInstance.getReference(ref).child("pwm").setValue(convertToPwm(seekBar.getProgress()));
                    mFirebaseInstance.getReference(ref).child("state").setValue(m_switchLight.isChecked());
                    mFirebaseInstance.getReference(ref).child("variable").setValue("light_channel_" + String.valueOf(m_spinnerLight.getSelectedItemPosition() + 1) + "_output_percents");
                    mFirebaseInstance.getReference(ref).child("index").setValue(m_spinnerLight.getSelectedItemPosition());

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });



    }

    private void updateListRecipeFromDatabase(final Spinner m_spinnerRecipe) {

        mFirebaseInstance.getReference("recipe").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                List<String> listRecipe = new ArrayList<>();

                for(DataSnapshot uniqueSnapshot : dataSnapshot.getChildren())
                {
                    String uuid = uniqueSnapshot.getKey();
                    String name = (String) uniqueSnapshot.getValue();
                    listRecipe.add(name);
                    m_myRecipeList.add(new RecipeClass(uuid, name));
                }

                updateSpinner(m_spinnerRecipe, listRecipe);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    private void updateSpinner(Spinner spinner, List<String> list){
        ArrayAdapter adapter = new ArrayAdapter<String>(Objects.requireNonNull(getContext()),  android.R.layout.simple_spinner_dropdown_item, list);
        adapter.setDropDownViewResource(android.R.layout.simple_list_item_single_choice);
        spinner.setAdapter(adapter);
    }

    private void updateWhenSwitch(final Switch sw, final String variable, final SeekBar seekBar, final int num, final int index_isFirstCall) {
        sw.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(!m_IsSwitchMotor.get(index_isFirstCall))
                {
                    if(isChecked) {
                        try {
                            updateConfig(variable, convertToPwm(seekBar.getProgress()), num);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        seekBar.setEnabled(true);

                    }
                    else {
                        try {
                            updateConfig(variable, 0, num);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        seekBar.setEnabled(false);

                    }
                }
                m_IsSwitchMotor.set(index_isFirstCall, false);
            }
        });
    }
    private void updateWhenChangeSeekBar(SeekBar seekBar, final TextView textView, final String actuatorVariable, final int num)
    {
        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                textView.setText(String.valueOf((int) (progress/65535.0 * 100)) +"%");
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
//                Toast.makeText(getContext(), "Stop", Toast.LENGTH_SHORT).show();
                try {
                    updateConfig(actuatorVariable, convertToPwm(seekBar.getProgress()), num);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }



    // For update config
    private class UpdateDeviceConfig extends AsyncTask<String, Void, String> {
        JSONObject data;
        public UpdateDeviceConfig(JSONObject data) throws JSONException {
            this.data = data;
        }

        @Override
        protected String doInBackground(String... urls)  {

            SendRequest request = new SendRequest("updateDeviceConfigHttps", this.data);
            try {
                return request.getResponse();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String result){
            try {
                JSONObject obj = new JSONObject(result);
                String error = obj.getString("error");
                if (error.equals("0")){
//                    Toast.makeText(getContext(),"", Toast.LENGTH_SHORT).show();
                }else{
                    Toast.makeText(getContext(),"Failed " + obj.getString("errorMessage"), Toast.LENGTH_SHORT).show();
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    private void updateConfig(String variable, int value, int number) throws JSONException {
        JSONObject data = new JSONObject();
        JSONObject ledJson = new JSONObject();
        JSONObject strConfig = new JSONObject();
        ledJson.put("message", "cmd_actuator");
        ledJson.put("variable", variable);
        ledJson.put("value", value);
        ledJson.put("number", number);
        String deviceId = "my-rasp-openag";

        data.put("deviceId", deviceId);
        data.put("updateConfig", this.encodeBase64(ledJson.toString()));

        new UpdateDeviceConfig(data).execute();

        // update in firebase

        if(!variable.contains("light")) {
            int state = 0;
            if (value == 0) state = 0;
            else state = 1;
            String ref = String.format("%s/curr_actuator/%s", deviceId, variable);
            mFirebaseInstance.getReference(ref).child("pwm").setValue(value);
            mFirebaseInstance.getReference(ref).child("state").setValue(state);
        }
    }

    private void updateMode(String variable) throws JSONException {
        JSONObject data = new JSONObject();
        JSONObject ledJson = new JSONObject();
        JSONObject strConfig = new JSONObject();
        ledJson.put("message", variable);
        String deviceId = "my-rasp-openag";

        data.put("deviceId", deviceId);
        data.put("updateConfig", this.encodeBase64(ledJson.toString()));

        new UpdateDeviceConfig(data).execute();
    }

    private void updateRecipe(String variable, String value, int index) throws JSONException {
        JSONObject data = new JSONObject();
        JSONObject ledJson = new JSONObject();
        JSONObject strConfig = new JSONObject();
        ledJson.put("message", "load_recipe");
        ledJson.put("variable", variable);
        ledJson.put("value", value);
        String deviceId = "my-rasp-openag";

        data.put("deviceId", deviceId);
        data.put("updateConfig", this.encodeBase64(ledJson.toString()));
        new UpdateDeviceConfig(data).execute();

        // update in firebase
        String ref = String.format("%s/curr_recipe", deviceId);
        mFirebaseInstance.getReference(ref).child("uuid").setValue(variable);
        mFirebaseInstance.getReference(ref).child("name").setValue(value);
        mFirebaseInstance.getReference(ref).child("index").setValue(index);
    }

    private String encodeBase64(String in) {
        byte[] encodeValue = Base64.encode(in.getBytes(), Base64.NO_WRAP);
        return new String(encodeValue);
    }

    // utility
    private int convertToPwm(int value){
        return value;
    }

    private int convertToProgress(int pwm)
    {
        return pwm;
    }
}
