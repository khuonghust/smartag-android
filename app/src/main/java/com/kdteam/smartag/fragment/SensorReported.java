package com.kdteam.smartag.fragment;

public class SensorReported {
    public double air_carbon_dioxide_ppm;
    public double air_humidity_percent;
    public double air_temperature_celsius;
    public double light_illumination_distance_cm;
    public double water_electrical_conductivity_ms_cm;
    public double water_potential_hydrogen;
    public double water_temperature_celsius;
    public double time_of_day;


    double min_dht_rh = 0;
    double max_dht_rh = 100;

    double min_dht_temp = 0;
    double max_dht_temp = 100;

    double min_water_temp = 0;
    double max_water_temp = 100;

    double min_light = 0.045;
    double max_light = 188000;

    double min_co2 = 0;
    double max_co2 = 2000;

    double min_ph = 0;
    double max_ph = 14;

    double min_ec = 1;
    double max_ec = 20; //ms/cm


    SensorReported()
    {

    }

    SensorReported(double air_carbon_dioxide_ppm, double air_humidity_percent,
                   double air_temperature_celsius, double light_illumination_distance_cm, double time_of_day, double water_electrical_conductivity_ms_cm,
                   double water_potential_hydrogen, double water_temperature_celsius)
    {
        this.air_carbon_dioxide_ppm = air_carbon_dioxide_ppm;
        this.air_humidity_percent = air_humidity_percent;
        this.air_temperature_celsius = air_temperature_celsius;
        this.light_illumination_distance_cm = light_illumination_distance_cm;
        this.water_electrical_conductivity_ms_cm = water_electrical_conductivity_ms_cm;
        this.water_potential_hydrogen = water_potential_hydrogen;
        this.water_temperature_celsius = water_temperature_celsius;
        this.time_of_day = time_of_day;
    }


    public int convertInRange(double min, double max, double val)
    {
        double range = max - min;
        return (int) (((val - min)/ range) *100);
    }

    public int get_air_temp_percent()
    {
        return convertInRange(min_dht_temp, max_dht_temp, air_temperature_celsius);
    }

    public int get_air_humidity_percent()
    {
        return convertInRange(min_dht_rh, max_dht_rh, air_humidity_percent);
    }

    public int get_water_temp_percent()
    {
        return convertInRange(min_water_temp, max_water_temp, water_temperature_celsius);
    }

    public int get_light_percent()
    {
        return convertInRange(min_light, max_light, light_illumination_distance_cm);

    }

    public int get_ec_percent()
    {
        return convertInRange(min_ec, max_ec, water_electrical_conductivity_ms_cm);
    }

    public int get_co2_percent()
    {
        return convertInRange(min_co2, max_co2, air_carbon_dioxide_ppm);
    }

    public int get_ph_percent()
    {
        return convertInRange(min_ph, max_ph, water_potential_hydrogen);
    }

}
